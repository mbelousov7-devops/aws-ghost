ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')"
OS="$(uname | tr '[:upper:]' '[:lower:]')"

if [ "${ARCH}" = "amd64" ]; then
    echo "X64 Architecture"

    KUBECTL_VERSION=1.29.2
    TERRAFORM_VERSION=1.7.5-1
    AWSCLI_VERSION=2.15.19
    AWSCLI_ARCH="x86_64"
fi
if [ "${ARCH}" = "arm64" ]; then 
    echo "ARM Architecture"

    KUBECTL_VERSION=1.29.2
    TERRAFORM_VERSION=1.7.5-1
    AWSCLI_VERSION=2.15.19
    AWSCLI_ARCH="aarch64"
fi

install_kubectl() {
  version=${KUBECTL_VERSION}
  checksum=${KUBECTL_CHECKSUM}

  url="https://storage.googleapis.com/kubernetes-release/release/v${version}/bin/${OS}/${ARCH}/kubectl"
  object=${url##*/}

  curl -sSLO $url
  install $object /usr/local/bin
  rm $object
  kubectl version --client
}

install_hashicorp() {
  wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    tee /etc/apt/sources.list.d/hashicorp.list
  apt update
}

install_terraform() {
  version=${TERRAFORM_VERSION}

  apt-get install -y terraform=$version || apt-cache madison terraform
  terraform -version
}

install_awscli() {
  version=${AWSCLI_VERSION}

  curl "https://awscli.amazonaws.com/awscli-exe-${OS}-${AWSCLI_ARCH}-${version}.zip" -o "awscliv2.zip"

  unzip awscliv2.zip
  ./aws/install
  rm -rf aws*
  sudo rm -rf /usr/local/aws-cli/*/*/dist/awscli/examples
  aws --version
}

set_certificates() {
    apt-get -o "Acquire::https::Verify-Peer=false" -o "Acquire::https::Verify-Host=false" update
    apt-get -o "Acquire::https::Verify-Peer=false" -o "Acquire::https::Verify-Host=false" install -y curl software-properties-common ca-certificates
    update-ca-certificates
}
