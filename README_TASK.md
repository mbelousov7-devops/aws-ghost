# aws-ghost

Self hosted Ghost Blog platform in AWS


## Architecture design

![Architecture design](./docs/ghost.png)


Ghost doesn’t support load-balanced clustering or multi-server setups of any description, there should only be one Ghost instance per site
https://ghost.org/docs/faq/clustering-sharding-multi-server

so
- use cloudfront
- choose the size of the prod ec2 with a reserve

- Route53 and ALB-TLS configuration are not implemented in demo due these configuration requires domain registration

### multy region disaster recovery design

![disaster recovery plan](./docs/drp.png)


## how to deploy 

Prereqisites
- aws user key id  and access key for terrafrorm 
- makefile
- docker

0. aws access config:

copy AWS admin keys into infrastructure/.env
```
AWS_ACCESS_KEY_ID=******
AWS_DEFAULT_REGION=us-east-1
AWS_SECRET_ACCESS_KEY=******
```
0. update s3 bucket names

**changeme**-aws-ghost-terraform-state-us-east-1 -> **<your_account_name>**-aws-ghost-terraform-state-us-east-1
**changeme**-aws-ghost-terraform-state-us-east-2 -> **<your_account_name>**-aws-ghost-terraform-state-us-east-2

in next files:

```
./infrastructure/init_scripts/terraform-s3-create-bucket-us-east-1.sh
./infrastructure/init_scripts/terraform-s3-create-bucket-us-east-2.sh

./vpc/configs/backend/ghost-us-east-1.sh
./vpc/configs/backend/ghost-us-east-2.sh

./infrastructure/terraform/infra/configs/backend/default-us-east-1.sh

./infrastructure/terraform/app/configs/backend/dev-us-east-1.sh
./infrastructure/terraform/app/configs/backend/dev-us-east-2.sh

```

1. buid docker shell

``` bash
cd shell
make shell
```

in shell export AWS credentials
``` shell
export $(cat /repo/infrastructure/.env | xargs)
```

2. create terraform backend s3 bucket and dynamodb for terrafrom lock

in shell:
``` shell
cd /repo/infrastructure/init_scripts/
./terraform-s3-create-bucket-us-east-1.sh
./terraform-s3-create-bucket-us-east-2.sh
./terraform-dynamodb-create-table-us-east-1.sh
./terraform-dynamodb-create-table-us-east-2.sh
```

3. deploy aws vpc&network resources(time ~3m)

in shell:
``` shell
cd /repo/infrastructure/terraform/vpc/
REGION=us-east-1 make apply-plan
REGION=us-east-2 make apply-plan
```

4. deploy aws infra(aurora rds) resources(time ~20m)

in shell:
``` shell
cd /repo/infrastructure/terraform/infra/
make apply-plan
```

5. deploy env depended ghost resources resources(time ~15m)

in shell:
``` shell
cd /repo/infrastructure/terraform/app/
ENV=dev make apply-plan
```
copy ghost adress from terraform output: **ghost_url**

6. chech ghost status
wait 5-10 min until ghost initialization

 - go to **ghost_url** to chech ghost status
 - go to **ghost_url/ghost** to check admin configs (username: test@exampletest.com(from variables.tf) and password in aws-secrets)


7. deploy QA env if necessary

in shell:
``` shell
cd /repo/infrastructure/terraform/app/
ENV=qa make apply-plan
```

## how to clean up installation

1. destroy aws resources:

in shell:
``` shell
cd /repo/infrastructure/terraform/app/
ENV=qa make destroy
ENV=dev make destroy
cd /repo/infrastructure/terraform/infra/
make destroy
cd /repo/infrastructure/terraform/vpc/
REGION=us-east-1 make destroy
REGION=us-east-2 make destroy
```

2. delete terrafrom  dynamod db tables and s3 bucket via aws console