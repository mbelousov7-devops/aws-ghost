
provider "aws" {
  region = var.region
  default_tags {
    tags = local.default_tags
  }
}

provider "aws" {
  region = var.region_primary
  alias  = "primary"
  default_tags {
    tags = merge(local.default_tags, { "Resiliency" = "PRIMARY" })
  }
}

provider "aws" {
  region = var.region_secondary
  alias  = "secondary"
  default_tags {
    tags = merge(local.default_tags, { "Resiliency" = "DISASTER RECOVERY" })
  }
}

terraform {
  backend "s3" {
    # see detailed bucket configuration in configs/backend/<ENV>.sh file
  }

  required_version = ">= 1.4.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.00"
    }
  }
}


