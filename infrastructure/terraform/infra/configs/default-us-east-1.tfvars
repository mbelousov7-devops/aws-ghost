
#################################################### db settings ##############################################################
db_cluster_count_primary   = 1
db_cluster_count_secondary = 0

db_cluster_size_primary   = 1
db_cluster_size_secondary = 1

db_deletion_protection = false
db_engine              = "aurora-mysql"
db_engine_version      = "8.0.mysql_aurora.3.04.0"
db_engine_family       = "aurora-mysql8.0"

db_enabled_cloudwatch_logs_exports = ["error", "slowquery"]

db_cluster_parameters_env = [
  {
    name         = "slow_query_log"
    value        = "1"
    apply_method = "immediate"
  },
  {
    name         = "general_log"
    value        = "1"
    apply_method = "immediate"
  },
]

db_instance_class_primary = null

//configs to create a Multi-AZ RDS cluster?
//db_storage_type = "io1"
//db_allocated_storage = 100
//db_iops = 1000

serverlessv2_scaling_configuration_primary = {
  min_capacity = 2
  max_capacity = 4
}

db_instance_class_secondary = null
serverlessv2_scaling_configuration_secondary = {
  min_capacity = 2
  max_capacity = 4
}

