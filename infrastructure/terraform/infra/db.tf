locals {
  db_cluster_identifier        = "ghost-${var.tf_stack}-${var.tf_zone}-cl-global"
  db_subnet_group_primary_name = "ghost-${var.tf_stack}-${var.tf_zone}-cl-primary"

  db_labels = merge(
    { env = var.tf_zone },
    { prefix = "ghost" },
    { stack = var.tf_stack }
  )
}


resource "aws_db_subnet_group" "default" {
  count = 1

  name        = local.db_subnet_group_primary_name
  description = "For Aurora cluster ${local.db_cluster_identifier}"
  subnet_ids  = local.primary_priv_subnets

}


resource "aws_rds_global_cluster" "default" {
  global_cluster_identifier = local.db_cluster_identifier
  deletion_protection       = var.db_deletion_protection
  engine                    = var.db_engine
  engine_version            = var.db_engine_version
  database_name             = var.tf_stack
  storage_encrypted         = var.db_storage_encrypted
  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_cloudwatch_log_group" "primary" {
  for_each          = toset(var.db_enabled_cloudwatch_logs_exports)
  name              = "/aws/rds/cluster/${local.db_labels.prefix}-${local.db_labels.stack}-${var.component_name_primary}-primary-${local.db_labels.env}/${each.key}"
  provider          = aws.primary
  retention_in_days = 30
}

resource "aws_sns_topic" "default" {
  name = "${local.db_cluster_identifier}-alarms"
}

module "aurora_mysql_rds_cluster_primary" {
  source = "./../modules/aws-rds-cluster"
  depends_on = [
    aws_rds_global_cluster.default,
    aws_cloudwatch_log_group.primary
  ]
  count                              = var.db_cluster_count_primary
  cluster_type                       = "primary"
  cluster_name                       = "primary"
  providers                          = { aws = aws.primary }
  vpc_id                             = data.aws_vpc.primary.id
  db_engine                          = var.db_engine
  db_engine_version                  = var.db_engine_version
  db_engine_family                   = var.db_engine_family
  auto_minor_version_upgrade         = true
  master_username                    = local.db_credentials.username
  master_password                    = local.db_credentials.password
  global_cluster_identifier          = aws_rds_global_cluster.default.id
  cluster_size                       = var.db_cluster_size_primary
  cluster_parameters                 = concat(var.db_cluster_parameters, var.db_cluster_parameters_env)
  instance_parameters                = var.db_instance_parameters
  serverlessv2_scaling_configuration = var.serverlessv2_scaling_configuration_primary
  source_region                      = var.region_primary
  availability_zones                 = var.availability_zones_primary
  storage_encrypted                  = var.db_storage_encrypted
  db_instance_class                  = var.db_instance_class_primary
  storage_type                       = var.db_storage_type
  iops                               = var.db_iops
  allocated_storage                  = var.db_allocated_storage
  kms_key_arn                        = null
  deletion_protection                = var.db_deletion_protection
  db_subnet_group_name               = join("", aws_db_subnet_group.default.*.id)
  security_group_cidr_blocks_allowed = local.primary_priv_cidr_blocks
  security_groups_ids_allowed        = []
  security_group_ids_add_external    = []
  labels                             = merge(local.db_labels, { component = var.component_name_primary }, )
  enabled_cloudwatch_logs_exports    = var.db_enabled_cloudwatch_logs_exports

  alarm_enable    = true
  alarm_topic_arn = aws_sns_topic.default.arn

}