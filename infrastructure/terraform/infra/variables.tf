//config from backend start
variable "region" {}
variable "vpc_name" {}
variable "tf_bucket" {}
variable "tf_stack" {}
variable "tf_zone" {}
variable "tf_key" {}
variable "dynamodb_table" {}
//config from backend end



############################################### regions and vpc's variables ############################################################
variable "region_primary" {
  type    = string
  default = "us-east-1"
}

variable "region_secondary" {
  type    = string
  default = "us-east-2"
}

variable "vpc_name_primary" {
  type    = string
  default = "vpc-ghost-us-east-1"
}

variable "vpc_name_secondary" {
  type    = string
  default = "vpc-ghost-us-east-2"
}

variable "availability_zones_primary" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b", "us-east-1c", ]
}

variable "availability_zones_secondary" {
  type    = list(string)
  default = ["us-east-2a", "us-east-2b", "us-east-2c"]
}

#################################################### db settings ##############################################################
variable "db_cluster_count_primary" {
  type    = number
  default = 1
}

variable "db_cluster_count_secondary" {
  type    = number
  default = 1
}

variable "db_cluster_size_primary" {
  type        = number
  default     = 1
  description = "Number of DB instances to create in the  primary cluster"
}

variable "db_cluster_size_secondary" {
  type        = number
  default     = 1
  description = "Number of DB instances to create in the  primary cluster"
}

variable "component_name_primary" {
  default     = "aurora-cl"
  description = "component name, using to change cluster name in case of reconfiguration"
}

variable "component_name_secondary" {
  default     = "aurora-cl"
  description = "component name, using to change cluster name in case of reconfiguration"
}

variable "db_deletion_protection" {
  type        = bool
  description = "If the DB instance should have deletion protection enabled"
  default     = false
}

variable "db_engine" {
  type        = string
  default     = "aurora-mysql"
  description = "The name of the database engine to be used for this DB cluster. Valid values: `aurora`, `aurora-mysql`, `aurora-postgresql`"
}

variable "db_engine_version" {
  type        = string
  description = "The version of the database engine to use. See `aws rds describe-db-engine-versions` "
}

variable "db_engine_family" {
  type        = string
  description = "The family of the DB cluster parameter group"
}

variable "db_storage_encrypted" {
  type        = bool
  description = "Specifies whether the DB cluster is encrypted."
  default     = true
}

variable "db_enabled_cloudwatch_logs_exports" {
  type        = list(string)
  description = "List of log types to export to cloudwatch. The following log types are supported: audit, error, general, slowquery"
  default     = ["error", ]
}

variable "db_username" {
  type        = string
  default     = "admin"
  description = "database master username"
}

variable "db_pass_version" {
  type        = number
  description = "Password version. Increment this to trigger a new password."
  default     = 1
}

variable "db_cluster_parameters" {
  type = list(object({
    apply_method = string
    name         = string
    value        = string
  }))
  default = [
    {
      name         = "require_secure_transport"
      value        = "OFF"
      apply_method = "immediate"
    },
    {
      name         = "time_zone"
      value        = "UTC"
      apply_method = "immediate"
    },
    {
      name         = "tls_version"
      value        = "TLSv1.2"
      apply_method = "pending-reboot"
    },
    {
      name         = "max_allowed_packet"
      value        = "1073741824"
      apply_method = "immediate"
    },
    {
      name         = "max_connections"
      value        = "4000"
      apply_method = "pending-reboot"
    },
    {
      name         = "thread_stack"
      value        = "512000"
      apply_method = "pending-reboot"
    },
  ]
  description = "List of default DB cluster parameters to apply"
}

variable "db_cluster_parameters_env" {
  type = list(object({
    apply_method = string
    name         = string
    value        = string
  }))
  default = [
  ]
  description = "List of additional(env depended) DB cluster parameters to apply"
}

variable "db_instance_parameters" {
  type = list(object({
    apply_method = string
    name         = string
    value        = string
  }))
  default     = []
  description = "List of DB instance parameters to apply"
}

variable "serverlessv2_scaling_configuration_primary" {
  type = object({
    min_capacity = number
    max_capacity = number
  })
  description = "serverlessv2 scaling properties"
}

variable "serverlessv2_scaling_configuration_secondary" {
  type = object({
    min_capacity = number
    max_capacity = number
  })
  description = "serverlessv2 scaling properties"
}

variable "db_instance_class_primary" {
  type        = string
  default     = null
  description = "This setting is required to create a provisioned Multi-AZ DB cluster"
}

variable "db_instance_class_secondary" {
  type        = string
  default     = null
  description = "This setting is required to create a provisioned Multi-AZ DB cluster"
}
variable "db_storage_type" {
  type        = string
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD)"
  default     = null
}

variable "db_iops" {
  type        = number
  description = <<-EOT
  The amount of provisioned IOPS. Setting this implies a storage_type of 'io1'. This setting is required to create a Multi-AZ DB cluster. 
  Check TF docs for values based on db engine
  EOT
  default     = null
}

variable "db_allocated_storage" {
  type        = number
  description = "The allocated storage in GBs"
  default     = null
}


