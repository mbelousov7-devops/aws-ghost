resource "aws_cloudfront_distribution" "ghost" {
  //aliases = ["${aws_lb.ghost.dns_name}"]

  count = var.cloudfront_count //deploy cloudfront_count = 1 for prod only

  origin {
    domain_name = aws_lb.ghost.dns_name

    origin_id = local.ghost_lb_name

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1.2", "TLSv1.1"]
    }
  }

  enabled         = true
  is_ipv6_enabled = false
  comment         = "Ghost ${var.tf_env} CloudFront Distribution"


  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.ghost_lb_name

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all" //"redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  tags = {
    Name = "ghost-${var.tf_env}"
  }
}