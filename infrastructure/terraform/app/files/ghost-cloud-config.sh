#cloud-config
system_info:
  default_user:
    name: ${ghost_ec2_username}
repo_update: true
repo_upgrade: all

runcmd:
  - export PATH=$PATH:/usr/local/bin
  - sudo apt-get update
  - sudo apt-get upgrade -y
  - sudo apt-get install -y curl sudo mysql-server nginx
  - sudo ufw allow 'Nginx Full'
  - sudo apt-get install -y ca-certificates curl gnupg
  - mkdir -p /etc/apt/keyrings
  - curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
  - curl -sL https://deb.nodesource.com/setup_18.x | sudo -E bash

  - sudo apt-get install nodejs -y
  
  - sudo npm install ghost-cli@latest -g
  - sudo mkdir -p /var/www/sitename
  - sudo chown ${ghost_ec2_username}:${ghost_ec2_username} /var/www/sitename
  - sudo chmod 775 /var/www/sitename
  - sudo su ${ghost_ec2_username} --command "cd /var/www/sitename && ghost install --no-prompt --no-setup-ssl --url '${url}' --port ${ghost_port} --db 'mysql' --dbhost '${dbhost}' --dbuser '${dbuser}' --dbpass '${dbpass}' --dbname '${dbname}'"
  - sudo su ${ghost_ec2_username} --command "cd /var/www/sitename && ghost start"

  - |
    curl 'http://localhost:${ghost_port}/ghost/api/admin/authentication/setup/' \
    -X 'POST' \
    -H 'Content-Type: application/json; charset=UTF-8' \
    -H 'Accept: application/json, text/javascript, */*; q=0.01' \
    -H 'Host: localhost:${ghost_port}' \
    -H 'Origin: http://localhost:${ghost_port}' \
    -H 'Referer: http://localhost:${ghost_port}/ghost/' \
    -H 'Connection: keep-alive' \
    -H 'X-Requested-With: XMLHttpRequest' \
    -H 'App-Pragma: no-cache' \
    --data-binary '{"setup":[{"name":"${ghost_admin_user}","email":"${ghost_admin_email}","password":"${ghost_admin_password}","blogTitle":"Blog Title"}]}'