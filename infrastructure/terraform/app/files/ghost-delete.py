import urllib.request
import json

def lambda_handler(event, context):
    # Get API keys from event parameters
    ghost_content_api_key = event.get('GHOST_CONTENT_API_KEY')
    ghost_admin_api_key = event.get('GHOST_ADMIN_API_KEY')

    # Check if API keys are provided
    if not ghost_content_api_key or not ghost_admin_api_key:
        return {
            'statusCode': 400,
            'body': 'GHOST_CONTENT_API_KEY and GHOST_ADMIN_API_KEY are required in event parameters'
        }

    # Get Ghost API URL from event parameters
    ghost_api_url = event.get('GHOST_API_URL')
    if not ghost_api_url:
        return {
            'statusCode': 400,
            'body': 'GHOST_API_URL is required in event parameters'
        }

    # Define headers for authentication
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'GhostContent {ghost_content_api_key}',
        'X-Ghost-Admin-API-Key': ghost_admin_api_key
    }

    # Get all posts
    posts_url = f"{ghost_api_url}/ghost/api/v3/content/posts/"
    req = urllib.request.Request(posts_url, headers=headers, method='GET')
    try:
        with urllib.request.urlopen(req) as response:
            posts_data = json.loads(response.read().decode('utf-8'))
    except urllib.error.HTTPError as e:
        return {
            'statusCode': e.code,
            'body': e.reason
        }

    # Delete each post
    for post in posts_data['posts']:
        post_id = post['id']
        delete_url = f"{ghost_api_url}/ghost/api/v3/admin/posts/{post_id}/"
        req = urllib.request.Request(delete_url, headers=headers, method='DELETE')
        try:
            with urllib.request.urlopen(req) as response:
                if response.getcode() == 204:
                    print(f"Deleted post with ID {post_id}")
                else:
                    print(f"Failed to delete post with ID {post_id}, status code: {response.getcode()}")
        except urllib.error.HTTPError as e:
            print(f"Failed to delete post with ID {post_id}, status code: {e.code}")

    return {
        'statusCode': 200,
        'body': 'All posts deleted successfully'
    }

