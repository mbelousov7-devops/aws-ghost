//config from backend start
variable "region" {}
variable "vpc_name" {}
variable "tf_bucket" {}
variable "tf_stack" {}
variable "tf_zone" {}
variable "tf_env" {}
variable "tf_key" {}
variable "dynamodb_table" {}
//config from backend end


variable "prefix" {
  type    = string
  default = "ghost"
}

############################################### regions and vpc's variables ############################################################

variable "availability_zones" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "cloudfront_count" {
  default = 0
}

############################################################ ghost ec2 variables ############################################################


variable "ghost_ec2_username" {
  type        = string
  description = "Username for the account:"
  default     = "ghost"
}

variable "ghost_port" {
  default = 25639
}

variable "ghost_admin_user" {
  default = "admin"
}

variable "ghost_admin_email" {
  default = "test@exampletest.com"
}

############################################################ ghost db variables ############################################################

variable "ghost_db_secret_name" {
  default = "ghost-infra-secrets-default"
}
 