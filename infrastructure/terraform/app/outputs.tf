
output "ghost_url" {
  value = local.ghost_url
}


output "ghost_url_cloudfront" {
  value = "https://${join("", aws_cloudfront_distribution.ghost.*.domain_name)}"
}


