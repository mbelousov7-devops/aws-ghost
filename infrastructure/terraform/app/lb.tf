locals {

  ghost_lb_component_name = "ghost-lb"
  ghost_lb_prefix         = "${var.prefix}-${var.tf_stack}-${local.ghost_lb_component_name}"
  ghost_lb_name           = var.tf_env
  ghost_tg_name           = "${local.ghost_prefix}-tg-${var.region}-${var.tf_env}"


  ghost_lb_security_group = {
    ingress_rules = [
      {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        description = "Allow HTTP access"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        description = "Allow HTTPs access"
        cidr_blocks = ["0.0.0.0/0"]
      },
    ]
    egress_rules = [
      {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        description = "Allow HTTP access"
        cidr_blocks = ["0.0.0.0/0"]
      },
      {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        description = "Allow HTTPs access"
        cidr_blocks = ["0.0.0.0/0"]
      }
    ]
  }
}

module "ghost_lb_security_group" {
  source        = "./../modules/aws-security-group"
  vpc_id        = data.aws_vpc.main.id
  ingress_rules = local.ghost_lb_security_group.ingress_rules
  egress_rules  = local.ghost_lb_security_group.egress_rules
  labels        = merge(local.labels, { component = local.ghost_lb_component_name }, )
}


resource "aws_lb" "ghost" {
  name               = local.ghost_lb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [module.ghost_lb_security_group.id]
  subnets            = local.primary_pub_subnets
}

resource "aws_lb_target_group" "ghost" {
  name     = local.ghost_tg_name
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.main.id
}

resource "aws_lb_listener" "ghost" {
  load_balancer_arn = aws_lb.ghost.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ghost.arn
  }
}
