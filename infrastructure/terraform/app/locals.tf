data "aws_caller_identity" "current" {}

data "aws_secretsmanager_secret" "ghost_db_secret_name" {
  name = var.ghost_db_secret_name
}

data "aws_secretsmanager_secret_version" "ghost_db_secret_name" {
  secret_id = data.aws_secretsmanager_secret.ghost_db_secret_name.id
}

locals {

  account_id = data.aws_caller_identity.current.account_id

  default_tags = {
    Provisioner = "Terraform"
    "TF:Stack"  = var.tf_stack
    "TF:Zone"   = var.tf_zone
    "TF:Env"    = var.tf_env
  }

  labels = merge(
    { env = var.tf_env },
    { prefix = var.prefix },
    { stack = var.tf_stack },
  )

}