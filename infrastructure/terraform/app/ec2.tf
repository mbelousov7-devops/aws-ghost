locals {
  ghost_component_name       = "ghost"
  ghost_prefix               = "${var.prefix}-${var.tf_stack}-${local.ghost_component_name}"
  ghost_ec2_iam_role_name    = "${local.ghost_prefix}-ec2-${var.region}-${var.tf_env}"
  ghost_ec2_iam_policy_name  = "${local.ghost_prefix}-ec2-${var.region}-${var.tf_env}"
  ghost_ec2_iam_profile_name = "${local.ghost_prefix}-ec2-${var.region}-${var.tf_env}"
  ghost_ec2_template_name    = "${local.ghost_prefix}-ec2-${var.region}-${var.tf_env}"

  ghost_dbhost         = jsondecode(data.aws_secretsmanager_secret_version.ghost_db_secret_name.secret_string)["endpoint"]
  ghost_dbport         = jsondecode(data.aws_secretsmanager_secret_version.ghost_db_secret_name.secret_string)["port"]
  ghost_dbuser         = jsondecode(data.aws_secretsmanager_secret_version.ghost_db_secret_name.secret_string)["username"]
  ghost_dbpass         = jsondecode(data.aws_secretsmanager_secret_version.ghost_db_secret_name.secret_string)["password"]
  ghost_dbname         = "ghost${var.tf_env}"
  ghost_port           = var.ghost_port
  ghost_url            = "http://${aws_lb.ghost.dns_name}"
  ghost_admin_user     = var.ghost_admin_user
  ghost_admin_email    = var.ghost_admin_email
  ghost_admin_password = local.ghost_dbpass

  ghost_security_group = {
    ingress_rules = [
      {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        description = "Allow HTTP access"
        cidr_blocks = local.primary_pub_cidr_blocks //["0.0.0.0/0"]
      },
      {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        description = "Allow HTTP access"
        cidr_blocks = local.primary_pub_cidr_blocks //["0.0.0.0/0"]
      },
      {
        from_port   = local.ghost_port
        to_port     = local.ghost_port
        protocol    = "tcp"
        description = "Allow Admin access"
        cidr_blocks = local.primary_priv_cidr_blocks
      },
    ]
    egress_rules = [
      {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        description = "Allow All access"
        cidr_blocks = ["0.0.0.0/0"]
      },
    ]
  }
}

#Search the AMI list for Ubuntu 20.04 Server
data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"] #Filter by Ubuntu 20.04 Server
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical (Creators of Ubuntu)
}

data "aws_iam_policy_document" "ghost" {
  statement {
    sid = ""
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
    effect = "Allow"
  }
}

resource "aws_iam_role" "ghost" {
  name = local.ghost_ec2_iam_role_name
  tags = merge(
    local.labels,
    { Name = local.ghost_ec2_iam_role_name },
  )
  assume_role_policy = data.aws_iam_policy_document.ghost.json
}



data "aws_iam_policy_document" "ghost_policy" {
  statement {
    effect = "Allow"

    actions = [
      "*"
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "ghost" {
  name   = local.ghost_ec2_iam_policy_name
  role   = aws_iam_role.ghost.id
  policy = data.aws_iam_policy_document.ghost_policy.json
}

resource "aws_iam_instance_profile" "ghost" {
  name = local.ghost_ec2_iam_profile_name
  role = aws_iam_role.ghost.name
  tags = merge(
    local.labels,
    { component = local.ghost_component_name },
    { Name = local.ghost_ec2_iam_profile_name },
  )
}


module "ghost_ec2_security_group" {
  source        = "./../modules/aws-security-group"
  vpc_id        = data.aws_vpc.main.id
  ingress_rules = local.ghost_security_group.ingress_rules
  egress_rules  = local.ghost_security_group.egress_rules
  labels        = merge(local.labels, { component = local.ghost_component_name }, )
}

resource "aws_launch_template" "ghost" {
  name_prefix   = "${local.ghost_prefix}-"
  image_id      = data.aws_ami.ubuntu.id
  instance_type = "t2.small"
  //key_name      = var.key_pair
  iam_instance_profile {
    name = aws_iam_instance_profile.ghost.name
  }
  vpc_security_group_ids = ["${module.ghost_ec2_security_group.id}"]
  update_default_version = true
  user_data = base64encode(templatefile("${path.module}/files/ghost-cloud-config.sh", {
    ghost_ec2_username   = var.ghost_ec2_username
    url                  = local.ghost_url
    dbhost               = local.ghost_dbhost
    dbport               = local.ghost_dbport
    dbuser               = local.ghost_dbuser
    dbpass               = local.ghost_dbpass
    dbname               = local.ghost_dbname
    ghost_port           = local.ghost_port
    ghost_admin_user     = local.ghost_admin_user
    ghost_admin_email    = local.ghost_admin_email
    ghost_admin_password = local.ghost_admin_password
    }
  ))
  tag_specifications {
    resource_type = "instance"

    tags = merge(
      local.labels,
      { component = local.ghost_component_name },
      { Name = local.ghost_ec2_template_name },
    )
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  count = 1

  name             = "asg-${aws_launch_template.ghost.name}"
  max_size         = 1
  min_size         = 1
  desired_capacity = 1

  vpc_zone_identifier = local.primary_priv_subnets
  //vpc_zone_identifier = local.primary_pub_subnets

  launch_template {
    id      = aws_launch_template.ghost.id
    version = "$Latest"
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 0
    }
    triggers = ["tag"]
  }

  target_group_arns = ["${aws_lb_target_group.ghost.arn}"]

}