locals {
  ghost_function_name          = "ghost-${var.tf_env}"
  ghost_function_iam_role_name = "ghost-${var.tf_env}"

}

data "archive_file" "ghost_lambda_function" {
  type        = "zip"
  source_file = "files/ghost-delete.py"
  output_path = ".terraform/ghost-delete.zip"
}

module "ghost_lambda_function" {
  source                 = "./../modules/aws-lambda-function"
  function_iam_role_path = "/${var.tf_stack}/"
  runtime                = "python3.11"
  handler                = "ghost-delete.lambda_handler"

  memory_size           = "256"
  timeout               = 120
  filename              = data.archive_file.ghost_lambda_function.output_path
  depends_on            = [data.archive_file.ghost_lambda_function]
  source_code_hash      = filebase64sha256(data.archive_file.ghost_lambda_function.output_path)
  aws_lambda_permission = {}
  lambda_environment = {
    variables = {
      GHOST_BASE_URL = local.ghost_url
      GHOST_USERNAME = local.ghost_admin_email
      GHOST_PASSWORD = local.ghost_admin_password
    }
  }
  vpc_config                      = null
  function_role_policy_statements = {}
  function_name                   = local.ghost_function_name
  function_iam_role_name          = local.ghost_function_iam_role_name
  labels = {
    prefix    = var.prefix
    stack     = var.tf_stack
    component = "lambda"
    region    = var.region
  }
}
